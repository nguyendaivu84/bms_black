$(function(){
	$(".tabs").delegate(".excludes", "click touchend",function(e){
		e.preventDefault();
		var group = $(this).data("group");
		var pid = $(this).data("pid");		
		var default_id = $("[name='"+group+"']").val();
		//chuyen action tren giao dien
		$("[data-group='"+group+"']").removeClass('option_item_active');
		$("[data-pid='"+pid+"']").addClass('option_item_active');
		$("[data-group-qty='"+group+"']").val(0).attr("value","0");
		$("input."+pid).val(1).attr("value","1");	
		if(group=='Size' && $("#cate_banhmisub").val()=='1'){ // thuoc banhmisub category			
			// mac dinh la` 11''
			var name = $("#name_"+pid).html();
			if(name=='11" Sub') {
				$("#cate_banhmisub_size").val(1);
			}else{
				$("#cate_banhmisub_size").val(0);
			}
		}
		let group_type = 'Inc';
		if($("#"+pid).attr("data-group-type")!=undefined){
			group_type = $("#"+pid).attr("data-group-type");
		}
		console.log(group);
		console.log(pid);
		console.log(default_id);
		console.log(group_type);
		//truong hop group EXC: xoa het cac line cung group tren description
		if(group_type=='Exc' && $("[data-groupname='"+group+"']").length){
			$("[data-groupname='"+group+"']").remove();
		}
		//Truong hop chon default
		if(pid==default_id){
			//neu da chon roi thi xoa
			if($('#od_'+pid).length)
				$('#od_'+pid).remove();
		//truong hop khac default
		}else{
			//neu da add roi thi xoa
			if($('#od_'+pid).length)
				$('#od_'+pid).html($("#name_"+pid).html());
			//khong thi them vao
			else
				$('.op_description').append('<p id="od_'+pid+'" data-groupname="'+group+'">'+$("#name_"+pid).html()+'</p>');
		}
		calPrice();
	});

	$(".tabs").delegate(".change_qty_img", "click touchend",function(e){
		e.preventDefault();		
		upQty($(this).attr("alt"));
	});

	$('.close_combo_bt').bind("touchend click", function(e){
		e.preventDefault();
		cancelCombo();
	});
	$('.next_group_bt').bind("touchend click", function(e){
		e.preventDefault();
		nextGroup();
	});
	$('.end_group_bt').bind("touchend click", function(e){
		e.preventDefault();
		endGroup();
	});

});	
